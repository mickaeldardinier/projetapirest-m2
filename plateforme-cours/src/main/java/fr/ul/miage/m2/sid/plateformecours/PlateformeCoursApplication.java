package fr.ul.miage.m2.sid.plateformecours;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.test.context.ActiveProfiles;

@SpringBootApplication
public class PlateformeCoursApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlateformeCoursApplication.class, args);
	}

}
