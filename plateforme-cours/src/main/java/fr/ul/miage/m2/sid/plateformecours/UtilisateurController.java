package fr.ul.miage.m2.sid.plateformecours;

import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.ul.miage.m2.sid.plateformecours.entity.Cours;
import fr.ul.miage.m2.sid.plateformecours.entity.Episode;
import fr.ul.miage.m2.sid.plateformecours.entity.Utilisateur;
import fr.ul.miage.m2.sid.plateformecours.repository.CoursRepository;
import fr.ul.miage.m2.sid.plateformecours.repository.EpisodeRepository;
import fr.ul.miage.m2.sid.plateformecours.repository.UtilisateurRepository;

@RestController
@EnableAutoConfiguration
public class UtilisateurController {

	@Autowired
	private UtilisateurRepository ur;

	@Autowired
	private CoursRepository cr;

	@Autowired
	private EpisodeRepository er;

	@Autowired
	private HttpSession httpSession;

	/*
	 * Page pour vérifier la connection sinon redirection vers la page de connexion
	 */
	@PostMapping("/connexion")
	public ResponseEntity<String> verificationConnexion(@RequestParam("email") String email) {
		Utilisateur u = ur.findByEmail(email);
		if (u != null) {
			httpSession.setAttribute("id", u.getId());
			httpSession.setAttribute("email", u.getEmail());
			return new ResponseEntity<String>(httpSession.getId(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
	}

	/*
	 * Page pour se déconnecter
	 */
	@GetMapping("/deconnexion")
	public void deconnexion() {
		httpSession.invalidate();
	}

	/*
	 * --------------------- PARTIE UTILISATEUR ---------------------
	 */

	/*
	 * Valider une inscription
	 */
	@PostMapping("/inscription")
	public ResponseEntity<Utilisateur> inscription(@RequestParam("email") String email, @RequestParam("nom") String nom,
			@RequestParam("prenom") String prenom) {
		Pattern pattern = Pattern.compile("^(.+)@(.+)$");
		if (pattern.matcher(email).matches()) {
			Utilisateur u = ur.save(new Utilisateur(email, nom, prenom));
			return new ResponseEntity<Utilisateur>(u, HttpStatus.OK);
		}
		return new ResponseEntity<Utilisateur>(HttpStatus.BAD_REQUEST);
	}

	/*
	 * Accéder à son profil
	 */
	@GetMapping("/profil")
	public ResponseEntity<Utilisateur> getProfil() {
		Object user = httpSession.getAttribute("id");
		if (user instanceof Long) {
			Long userId = (Long) user;
			Optional<Utilisateur> u = ur.findById(userId);
			if (u.isPresent()) {
				return new ResponseEntity<Utilisateur>(u.get(), HttpStatus.OK);
			}
		}
		return new ResponseEntity<Utilisateur>(HttpStatus.FORBIDDEN);
	}

	/*
	 * Voir les cours achetés
	 */
	@GetMapping("/bibliotheque")
	public ResponseEntity<Set<Cours>> listeCoursAchetes() {
		Object userSession = httpSession.getAttribute("id");
		if (userSession instanceof Long) {
			Long userId = (Long) userSession;
			Optional<Utilisateur> u = ur.findById(userId);
			if (u.isPresent()) {
				Set<Cours> listeCours = u.get().getCours();
				for (Cours c : listeCours) {
					Link link = WebMvcLinkBuilder.linkTo(UtilisateurController.class).slash("bibliotheque")
							.slash(c.getId()).withSelfRel();
					c.add(link);
				}
				return new ResponseEntity<Set<Cours>>(listeCours, HttpStatus.OK);
			}
		}
		return new ResponseEntity<Set<Cours>>(HttpStatus.FORBIDDEN);
	}

	/*
	 * Visionner un cours acheté et la liste des épisodes
	 */
	@GetMapping("/bibliotheque/{id}")
	public ResponseEntity<Set<Episode>> contenuCoursAchete(@PathVariable Long id) {
		Optional<Cours> c = cr.findById(id);
		if (c.isPresent()) {
			Cours cours = c.get();
			Object userSession = httpSession.getAttribute("id");
			if (userSession instanceof Long) {
				Long userId = (Long) userSession;
				Optional<Utilisateur> u = ur.findById(userId);
				if (u.isPresent()) {
					if (u.get().getCours().contains(cours)) {
						Set<Episode> episodes = cours.getEpisodes();
						for (Episode e : episodes) {
							Link link = WebMvcLinkBuilder.linkTo(UtilisateurController.class).slash("bibliotheque")
									.slash(cours.getId()).slash(e.getId()).withSelfRel();
							e.add(link);
						}
						return new ResponseEntity<Set<Episode>>(episodes, HttpStatus.OK);
					}
				}
			}
			return new ResponseEntity<Set<Episode>>(HttpStatus.FORBIDDEN);
		}
		return new ResponseEntity<Set<Episode>>(HttpStatus.BAD_REQUEST);
	}

	/*
	 * Visionner un épisode d'un cours acheté
	 */
	@GetMapping("/bibliotheque/{idCours}/{idEpisode}")
	public ResponseEntity<Episode> contenuEpisodeAchete(@PathVariable Long idCours, @PathVariable Long idEpisode,
			HttpSession httpSession) {
		Optional<Cours> cours = cr.findById(idCours);
		if (cours.isPresent()) {
			Object userSession = httpSession.getAttribute("id");
			if (userSession instanceof Long) {
				Long userId = (Long) userSession;
				Optional<Utilisateur> u = ur.findById(userId);
				if (u.isPresent()) {
					if (u.get().getCours().contains(cours.get())) {
						Optional<Episode> e = er.findById(idEpisode);
						if (e.isPresent()) {
							if (cours.get().getEpisodes().contains(e.get())) {
								return new ResponseEntity<Episode>(e.get(), HttpStatus.OK);
							}
						}
					}
					return new ResponseEntity<Episode>(HttpStatus.BAD_REQUEST);
				}
			}
			return new ResponseEntity<Episode>(HttpStatus.FORBIDDEN);
		}
		return new ResponseEntity<Episode>(HttpStatus.BAD_REQUEST);
	}

}
