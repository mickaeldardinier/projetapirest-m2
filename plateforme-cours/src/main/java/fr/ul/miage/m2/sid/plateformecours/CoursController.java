package fr.ul.miage.m2.sid.plateformecours;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.ul.miage.m2.sid.plateformecours.entity.Cours;
import fr.ul.miage.m2.sid.plateformecours.entity.Episode;
import fr.ul.miage.m2.sid.plateformecours.entity.Utilisateur;
import fr.ul.miage.m2.sid.plateformecours.repository.CoursRepository;
import fr.ul.miage.m2.sid.plateformecours.repository.UtilisateurRepository;

@RestController
@EnableAutoConfiguration
@RequestMapping("/cours")
public class CoursController {

	@Autowired
	private UtilisateurRepository ur;

	@Autowired
	private CoursRepository cr;

	@Autowired
	private HttpSession httpSession;

	/*
	 * Visualiser la liste des cours
	 */
	@GetMapping
	public List<Cours> listeCours() {
		List<Cours> listeCours = cr.findAll();
		for (Cours c : listeCours) {
			Link link = WebMvcLinkBuilder.linkTo(CoursController.class).slash(c.getId()).withSelfRel();
			c.add(link);
		}
		return listeCours;
	}

	/*
	 * Voir le contenu d'un cours
	 */
	@GetMapping("/{id}")
	public ResponseEntity<Set<Episode>> voirCours(@PathVariable Long id) {
		Optional<Cours> c = cr.findById(id);
		if (c.isPresent()) {
			Set<Episode> episodes = c.get().getEpisodes();
			for (Episode e : episodes) {
				Link link = WebMvcLinkBuilder.linkTo(UtilisateurController.class).slash("bibliotheque")
						.slash(c.get().getId()).slash(e.getId()).withSelfRel();
				e.add(link);
			}
			return new ResponseEntity<Set<Episode>>(episodes, HttpStatus.OK);
		}
		return new ResponseEntity<Set<Episode>>(HttpStatus.BAD_REQUEST);
	}

	/*
	 * Acheter d'un cours
	 */
	@PostMapping("/{idCours}/achat")
	public ResponseEntity<String> validerAchatCours(@RequestParam("numeroCarte") String numeroCarte,
			@RequestParam("dateExpiration") String dateExpiration, @RequestParam("codeCarte") String codeCarte,
			@PathVariable Long idCours) {
		Object user = httpSession.getAttribute("id");
		if (user instanceof Long) {
			Long userId = (Long) user;
			if (VerificationCarteBancaire.transactionBancaireValide(numeroCarte, dateExpiration, codeCarte)) {
				Optional<Utilisateur> u = ur.findById(userId);
				Optional<Cours> c = cr.findById(idCours);
				if (u.isPresent() && c.isPresent()) {
					if (u.get().getCours().contains(c.get())) {
						return new ResponseEntity<String>("Vous possédez déjà ce cours", HttpStatus.BAD_REQUEST);
					}
					u.get().getCours().add(c.get());
					ur.save(u.get());
					return new ResponseEntity<String>("Achat validé", HttpStatus.OK);
				}
			}
			return new ResponseEntity<String>("Erreur dans les paramètres de la carte bancaire",
					HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>("Vous devez être connecté", HttpStatus.FORBIDDEN);
	}
}
