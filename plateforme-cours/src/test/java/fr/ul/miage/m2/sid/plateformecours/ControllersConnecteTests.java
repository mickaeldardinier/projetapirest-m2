package fr.ul.miage.m2.sid.plateformecours;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { PlateformeCoursApplication.class })
@AutoConfigureMockMvc
@WebAppConfiguration
@ActiveProfiles("test")
class ControllersConnecteTests {

	@Autowired
	private MockMvc mvc;

	/*
	 * Voir la liste des cours en étant connecté
	 */
	@Test
	public void accesCoursConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/cours").sessionAttr("id", Long.valueOf("1")).sessionAttr("email",
				"pierre.dupont@email.fr")).andExpect(MockMvcResultMatchers.status().isOk());
	}

	/*
	 * Voir la liste des cours, identifiant de session faux
	 */
	@Test
	public void accesCoursConnecteInconnu() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/cours").sessionAttr("id", Long.valueOf("5")).sessionAttr("email",
				"incconu@email.fr")).andExpect(MockMvcResultMatchers.status().isOk());
	}

	/*
	 * Test de la connection, avec un utilisateur existant
	 */
	@Test
	public void connection() throws Exception {
		String email = "pierre.dupont@email.fr";
		mvc.perform(MockMvcRequestBuilders.post("/connexion").param("email", email))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	/*
	 * Test de la connection, utilisateur inexistant
	 */
	@Test
	public void connectionInexistant() throws Exception {
		String email = "inconnu@email.fr";
		mvc.perform(MockMvcRequestBuilders.post("/connexion").param("email", email))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

	/*
	 * Avec une connection donc autorisé
	 */
	@Test
	public void accesProfilConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/profil").sessionAttr("id", Long.valueOf("1")).sessionAttr("email",
				"pierre.dupont@email.fr")).andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content()
						.json("{'id': 1,'email': 'pierre.dupont@email.fr','nom': 'Pierre','prenom': 'Dupont', 'statut': 'admin'}"));
	}

	/*
	 * Achat d'un cours
	 */
	@Test
	public void achatCours() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/cours/3/achat").param("numeroCarte", "123456781234")
				.param("dateExpiration", "12/2022").param("codeCarte", "1234").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr")).andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void achatCoursDejaAchete() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/cours/1/achat").param("numeroCarte", "123456781234")
				.param("dateExpiration", "12/2022").param("codeCarte", "1234").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr")).andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
	
	@Test
	public void achatCoursInexistant() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/cours/99/achat").param("numeroCarte", "123456781234")
				.param("dateExpiration", "12/2022").param("codeCarte", "1234").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr")).andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
	
	@Test
	public void achatCoursMauvaiseCodeCarte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/cours/1/achat").param("numeroCarte", "123456781234")
				.param("dateExpiration", "12/2022").param("codeCarte", "1111").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr")).andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
	
	@Test
	public void accesBibliotheque() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/bibliotheque").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr"))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void accesBibliothequeUnCours() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/bibliotheque/1").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr"))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void accesBibliothequeCoursNonPossede() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/bibliotheque/4").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr"))
				.andExpect(MockMvcResultMatchers.status().isForbidden());
	}
	
	@Test
	public void accesBibliothequeEpisodePasDansBonCours() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/bibliotheque/1/12").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr"))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
}
