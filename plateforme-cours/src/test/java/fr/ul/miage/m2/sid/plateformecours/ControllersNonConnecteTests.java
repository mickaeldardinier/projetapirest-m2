package fr.ul.miage.m2.sid.plateformecours;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { PlateformeCoursApplication.class })
@AutoConfigureMockMvc
@WebAppConfiguration
@ActiveProfiles("test")
class ControllersNonConnecteTests {

	@Autowired
	private MockMvc mvc;

	/*
	 * Actions en étant pas connecté
	 */
	@Test
	public void accesProfilNonConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/profil")).andExpect(MockMvcResultMatchers.status().isForbidden());
	}

	@Test
	public void accesBibliothequeNonConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/bibliotheque"))
				.andExpect(MockMvcResultMatchers.status().isForbidden());
	}

	@Test
	public void accesEpisodeNonConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/bibliotheque/1/1"))
				.andExpect(MockMvcResultMatchers.status().isForbidden());
	}

	@Test
	public void accesAchatNonConnecteCarteValide() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/cours/1/achat").param("numeroCarte", "123456781234")
				.param("dateExpiration", "12/2022").param("codeCarte", "1234"))
				.andExpect(MockMvcResultMatchers.status().isForbidden());
	}

	@Test
	public void accesListeCoursNonConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/cours")).andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void accesUnCoursNonConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/cours/1")).andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void accesUnCoursInexistant() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/cours/99")).andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

	@Test
	public void inscription() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/inscription").param("email", "mickael@email.fr")
				.param("nom", "dardinier").param("prenom", "mickael")).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void inscriptionMauvaise() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/inscription").param("email", "MauvaisFormatEmail")
				.param("nom", "dardinier").param("prenom", "mickael"))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
}
