package fr.ul.miage.m2.sid.plateformecours;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.ul.miage.m2.sid.plateformecours.entity.Cours;
import fr.ul.miage.m2.sid.plateformecours.entity.Episode;
import fr.ul.miage.m2.sid.plateformecours.repository.CoursRepository;
import fr.ul.miage.m2.sid.plateformecours.repository.EpisodeRepository;

@RestController
@EnableAutoConfiguration
@RequestMapping("/cours")
public class CoursController {

	@Autowired
	private CoursRepository cr;

	@Autowired
	private EpisodeRepository er;

	@Autowired
	private HttpSession httpSession;

	/*
	 * Visualiser la liste des cours
	 */
	@GetMapping
	public ResponseEntity<List<Cours>> listeCours() {
		if (httpSession.getAttribute("id") != null) {
			List<Cours> listeCours = cr.findAll();
			for (Cours c : listeCours) {
				Link link = WebMvcLinkBuilder.linkTo(CoursController.class).slash(c.getId()).withSelfRel();
				c.add(link);
			}
			return new ResponseEntity<List<Cours>>(listeCours, HttpStatus.OK);
		}
		return new ResponseEntity<List<Cours>>(HttpStatus.FORBIDDEN);
	}

	/*
	 * Voir le contenu d'un cours
	 */
	@GetMapping("/{id}")
	public ResponseEntity<Cours> voirCours(@PathVariable Long id) {
		if (httpSession.getAttribute("id") != null) {
			Optional<Cours> c = cr.findById(id);
			if (c.isPresent()) {
				Set<Episode> episodes = c.get().getEpisodes();
				for (Episode e : episodes) {
					Link link = WebMvcLinkBuilder.linkTo(CoursController.class).slash(c.get().getId()).slash(e.getId())
							.withSelfRel();
					e.add(link);
				}
				return new ResponseEntity<Cours>(c.get(), HttpStatus.OK);
			}
			return new ResponseEntity<Cours>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Cours>(HttpStatus.FORBIDDEN);
	}

	/*
	 * Créer un cours
	 */
	@PostMapping("/create")
	public ResponseEntity<String> creerCours(@RequestParam("titre") String titre,
			@RequestParam("description") String description, @RequestParam("prix") Double prix) {
		if (httpSession.getAttribute("id") != null) {
			Cours c = new Cours(titre, description, prix);
			cr.save(c);
			return new ResponseEntity<String>("Le cours a bien été crée", HttpStatus.OK);
		}
		return new ResponseEntity<String>("Vous devez être connecté", HttpStatus.FORBIDDEN);
	}

	/*
	 * Mettre à jour un cours
	 */
	@PutMapping("/{id}/update")
	public ResponseEntity<String> updateCours(@PathVariable Long id, @RequestParam("titre") String titre,
			@RequestParam("description") String description, @RequestParam("prix") Double prix) {
		if (httpSession.getAttribute("id") != null) {
			Optional<Cours> cours = cr.findById(id);
			if (cours.isPresent()) {
				Cours c = cours.get();
				c.setTitre(titre);
				c.setDescription(description);
				c.setPrix(prix);
				cr.save(c);
				return new ResponseEntity<String>("Le cours a bien été mis à jour", HttpStatus.OK);
			}
			return new ResponseEntity<String>("Ce cours n'existe pas", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>("Vous devez être connecté", HttpStatus.FORBIDDEN);
	}

	/*
	 * Supprimer un cours
	 */

	/*
	 * Visionner un épisode d'un cours
	 */
	@GetMapping("/{idCours}/{idEpisode}")
	public ResponseEntity<Episode> contenuEpisodeAchete(@PathVariable Long idCours, @PathVariable Long idEpisode) {
		if (httpSession.getAttribute("id") != null) {
			Optional<Cours> c = cr.findById(idCours);
			if (c.isPresent()) {
				Optional<Episode> e = er.findById(idEpisode);
				if (e.isPresent()) {
					if (c.get().getEpisodes().contains(e.get())) {
						return new ResponseEntity<Episode>(e.get(), HttpStatus.OK);
					}
				}
			}
			return new ResponseEntity<Episode>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Episode>(HttpStatus.FORBIDDEN);
	}

	/*
	 * Créer un épisode pour l'ajouter à un cours
	 */
	@PostMapping("/{id}/create")
	public ResponseEntity<String> creerEpisode(@PathVariable Long id, @RequestParam("nom") String nom,
			@RequestParam("lien") String lien) {
		if (httpSession.getAttribute("id") != null) {
			Episode e = new Episode(nom, lien);
			er.save(e);
			Optional<Cours> c = cr.findById(id);
			if (c.isPresent()) {
				c.get().getEpisodes().add(e);
				cr.save(c.get());
				return new ResponseEntity<String>("L'épisode a bien été crée", HttpStatus.OK);
			}
			return new ResponseEntity<String>("Ce cours n'existe pas", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<String>("Vous devez être connecté", HttpStatus.FORBIDDEN);
	}

	/*
	 * Mettre à jour un épisode
	 */
	@PutMapping("/{idCours}/{idEpisode}/update")
	public ResponseEntity<String> updateEpisode(@PathVariable Long idCours, @PathVariable Long idEpisode,
			@RequestParam("nom") String nom, @RequestParam("lien") String lien) {
		if (httpSession.getAttribute("id") != null) {
			Optional<Cours> c = cr.findById(idCours);
			if (c.isPresent()) {
				Optional<Episode> episode = er.findById(idEpisode);
				if (episode.isPresent()) {
					Episode e = episode.get();
					if (c.get().getEpisodes().contains(e)) {
						e.setNom(nom);
						e.setLien(lien);
						er.save(e);
						return new ResponseEntity<String>("L'épisode a bien été mis à jour", HttpStatus.OK);
					}
					return new ResponseEntity<String>("Cet épisode ne fait pas parti de ce cours",
							HttpStatus.BAD_REQUEST);
				}
				return new ResponseEntity<String>("Cet épisode n'existe pas", HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<String>("Ce cours n'existe pas", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<String>("Vous devez être connecté", HttpStatus.FORBIDDEN);
	}

	/*
	 * Supprimer un épisode
	 */

}
