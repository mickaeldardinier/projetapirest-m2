package fr.ul.miage.m2.sid.plateformecours;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.ul.miage.m2.sid.plateformecours.entity.Utilisateur;
import fr.ul.miage.m2.sid.plateformecours.repository.UtilisateurRepository;

@RestController
@EnableAutoConfiguration
public class AdminController {

	@Autowired
	private UtilisateurRepository ur;

	@Autowired
	private HttpSession httpSession;

	/*
	 * --------------------- PARTIE ADMINISTRATEUR ---------------------
	 */

	/*
	 * Page pour vérifier la connection sinon redirection vers la page de connexion
	 */
	@PostMapping("/connexion")
	public ResponseEntity<String> verificationConnexion(@RequestParam("email") String email) {
		Utilisateur u = ur.findByEmail(email);
		if (u != null) {
			if (u.getStatut().equals("admin")) {
				httpSession.setAttribute("id", u.getId());
				httpSession.setAttribute("email", u.getEmail());
				return new ResponseEntity<String>(httpSession.getId(), HttpStatus.OK);
			}
			return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);
		}
		return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
	}

	/*
	 * Page pour se déconnecter
	 */
	@GetMapping("/deconnexion")
	public void deconnexion() {
		httpSession.invalidate();
	}
}