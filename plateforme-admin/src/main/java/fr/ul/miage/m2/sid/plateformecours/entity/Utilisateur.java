package fr.ul.miage.m2.sid.plateformecours.entity;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "utilisateur")
public class Utilisateur extends RepresentationModel<Utilisateur> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(unique=true)
	private String email;

	private String nom;

	private String prenom;
	
	private String statut;

	@ManyToMany
	@JoinTable(name = "Utilisateur_Cours", joinColumns = { @JoinColumn(name = "id") }, inverseJoinColumns = {
			@JoinColumn(name = "ID_COURS") })
	@JsonIgnore
	private Set<Cours> cours;

	public Utilisateur(String email, String nom, String prenom) {
		setEmail(email);
		setNom(nom);
		setPrenom(prenom);
		setCours(new HashSet<Cours>());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Utilisateur other = (Utilisateur) obj;
		if (cours == null) {
			if (other.cours != null)
				return false;
		}
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		// Modification pour comparer uniquement le contenu des cours achetés un par un
		if (cours.size() != other.cours.size()) {
			return false;
		}
		for (Iterator<Cours> it = other.cours.iterator(); it.hasNext();) {
			if (!cours.contains(it.next())) {
				return false;
			}
		}

		return true;
	}
}
