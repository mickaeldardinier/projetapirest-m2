package fr.ul.miage.m2.sid.plateformecours;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.ul.miage.m2.sid.plateformecours.entity.Utilisateur;
import fr.ul.miage.m2.sid.plateformecours.repository.UtilisateurRepository;

@RestController
@EnableAutoConfiguration
@RequestMapping("/utilisateurs")
public class UtilisateursController {

	@Autowired
	private UtilisateurRepository ur;

	@Autowired
	private HttpSession httpSession;

	/*
	 * Consulter la liste des utilisateurs existants
	 */
	@GetMapping
	public ResponseEntity<List<Utilisateur>> getUtilisateurs() {
		if (httpSession.getAttribute("id") != null) {
			List<Utilisateur> utilisateurs = ur.findAll();
			for (Utilisateur u : utilisateurs) {
				Link link = WebMvcLinkBuilder.linkTo(UtilisateursController.class).slash(u.getId()).withSelfRel();
				u.add(link);
			}
			return new ResponseEntity<List<Utilisateur>>(utilisateurs, HttpStatus.OK);
		}
		return new ResponseEntity<List<Utilisateur>>(HttpStatus.FORBIDDEN);
	}

	/*
	 * Consulter les informations d'un utilisateur par son ID
	 */
	@GetMapping("/{id}")
	public ResponseEntity<Utilisateur> getUtilisateurbyId(@PathVariable Long id) {
		if (httpSession.getAttribute("id") != null) {
			Optional<Utilisateur> u = ur.findById(id);
			if (u.isPresent()) {
				return new ResponseEntity<Utilisateur>(u.get(), HttpStatus.OK);
			}
			return new ResponseEntity<Utilisateur>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Utilisateur>(HttpStatus.FORBIDDEN);
	}

	/*
	 * Mettre à jour un utilisateur
	 */
	@PutMapping("/{id}/update")
	public ResponseEntity<String> updateUtilisateur(@PathVariable Long id, @RequestParam("nom") String nom,
			@RequestParam("prenom") String prenom) {
		if (httpSession.getAttribute("id") != null) {
			Optional<Utilisateur> utilisateur = ur.findById(id);
			if (utilisateur.isPresent()) {
				Utilisateur u = utilisateur.get();
				u.setNom(nom);
				u.setPrenom(prenom);
				ur.save(u);
				return new ResponseEntity<String>("L'utilisateur a bien été mis à jour", HttpStatus.OK);
			}
			return new ResponseEntity<String>("Cet utilisateur n'existe pas", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>("Vous devez être connecté", HttpStatus.FORBIDDEN);
	}

	/*
	 * Supprimer un utilisateur (changer son statut)
	 */
}
