package fr.ul.miage.m2.sid.plateformecours;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import fr.ul.miage.m2.sid.plateformecours.PlateformeCoursApplication;
import fr.ul.miage.m2.sid.plateformecours.entity.Cours;
import fr.ul.miage.m2.sid.plateformecours.entity.Episode;
import fr.ul.miage.m2.sid.plateformecours.entity.Utilisateur;
import fr.ul.miage.m2.sid.plateformecours.repository.CoursRepository;
import fr.ul.miage.m2.sid.plateformecours.repository.UtilisateurRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { PlateformeCoursApplication.class })
@AutoConfigureTestDatabase
@Transactional
@ActiveProfiles("test")
class UtilisateurTests {

	@Resource
	private UtilisateurRepository ur;

	@Resource
	private CoursRepository cr;

	private Utilisateur u;

	private String email;

	@BeforeEach
	public void initialize() {
		email = "test@email.com";
		u = new Utilisateur(Long.valueOf("1"), email, "TestNom", "TestPrenom", "user", new HashSet<Cours>());
	}

	@Test
	void sauvegarderUser() {
		Utilisateur u2 = ur.save(u);
		assertEquals(u, u2);
	}

	@Test
	void chercherUserId() {
		ur.save(u);
		Utilisateur u3 = ur.findById(Long.valueOf("1")).get();
		assertEquals(u, u3);
	}

	@Test
	void chercherUserEmail() {
		ur.save(u);
		Utilisateur u4 = ur.findByEmail(email);
		assertEquals(u, u4);
	}

	@Test
	void updateUser() {
		ur.save(u);
		Utilisateur u5 = ur.findByEmail(email);
		u5.setPrenom("PrenomModifié");
		ur.save(u5);
		Utilisateur u6 = ur.findByEmail(email);
		assertEquals(u5, u6);
	}

	@Test
	void listerCours() {
		Cours c = new Cours(Long.valueOf("1"), "Titre", "Description", 10.5, new HashSet<Utilisateur>(), new HashSet<Episode>());
		u.getCours().add(c);
		cr.save(c);
		ur.save(u);
		Set<Cours> listeCours = ur.findById(Long.valueOf("1")).get().getCours();
		assertTrue(listeCours.contains(c));
		assertEquals(1, listeCours.size());
	}

	@Test
	void listerAucunCours() {
		Set<Cours> listeCours = ur.findById(Long.valueOf("2")).get().getCours();
		assertEquals(0, listeCours.size());
	}
	
//	@Test
//	void delete() {
//		Utilisateur u = new Utilisateur(Long.valueOf("4"), "email@email.fr", "remi", "jean", new HashSet<Cours>());
//		Cours c = new Cours(Long.valueOf("1"), "Bidule", "Non menfou", 4.1, new HashSet<Utilisateur>(), new HashSet<Episode>());
//		
//		ur.save(u);
//		u.getCours().add(c);
//		c.getUtilisateurs().add(u);
//		Cours c2 = cr.save(c);
//		Utilisateur r = ur.findById(Long.valueOf("4")).get();
//	//	System.out.println(c2.getId() +" "+c2.getTitre() + " "+c2.getUtilisateurs().get(0).getEmail());
////		assertEquals(c, c2);
////		assertEquals(u, r);
//	//	Cours rc = cr.findById(Long.valueOf("5")).get();
////		assertEquals(c, rc);
//		u.getCours().remove(0);
//		ur.save(u);
//		ur.delete(u);
//		
//		Utilisateur r2 = ur.findById(Long.valueOf("4")).get();
//		Cours rc2 = cr.findById(Long.valueOf("1")).get();
//	//	System.out.println(rc2.getId() +" "+rc2.getTitre() + " "+rc2.getUtilisateurs().get(0).getEmail());
//		assertEquals(0, rc2.getUtilisateurs().size());
//	}
}
