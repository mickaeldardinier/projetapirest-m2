package fr.ul.miage.m2.sid.plateformecours;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { PlateformeCoursApplication.class })
@AutoConfigureMockMvc
@WebAppConfiguration
@ActiveProfiles("test")
class ControllersConnecteTests {

	@Autowired
	private MockMvc mvc;

	@Test
	public void connection() throws Exception {
		String email = "pierre.dupont@email.fr";
		mvc.perform(MockMvcRequestBuilders.post("/connexion").param("email", email))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void connectionInexistant() throws Exception {
		String email = "inconnu@email.fr";
		mvc.perform(MockMvcRequestBuilders.post("/connexion").param("email", email))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

	@Test
	public void connectionPasAdmin() throws Exception {
		String email = "jeannefleur@mail.com";
		mvc.perform(MockMvcRequestBuilders.post("/connexion").param("email", email))
				.andExpect(MockMvcResultMatchers.status().isUnauthorized());
	}

	@Test
	public void accesUtilisateursConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/utilisateurs").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr")).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void accesUtilisateurConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/utilisateurs/1").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr")).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void accesUtilisateurInconnuConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/utilisateurs/99").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr")).andExpect(MockMvcResultMatchers.status().isNotFound());
	}

	@Test
	public void updateUtilisateurConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.put("/utilisateurs/2/update").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr").param("nom", "Testnom").param("prenom", "testPrenom"))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void updateUtilisateurInexistantConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.put("/utilisateurs/99/update").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr").param("nom", "Testnom").param("prenom", "testPrenom"))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

	@Test
	public void accesListeCoursConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/cours").sessionAttr("id", Long.valueOf("1")).sessionAttr("email",
				"pierre.dupont@email.fr")).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void accesCoursConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/cours/1").sessionAttr("id", Long.valueOf("1")).sessionAttr("email",
				"pierre.dupont@email.fr")).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void accesCoursInexistantConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/cours/99").sessionAttr("id", Long.valueOf("1")).sessionAttr("email",
				"pierre.dupont@email.fr")).andExpect(MockMvcResultMatchers.status().isNotFound());
	}

	@Test
	public void creerCoursConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/cours/create").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr").param("titre", "Cours sur les tests JUNIT")
				.param("description", "Cours portant sur comment bien réaliser ces tests avec JUNIT")
				.param("prix", "45.5")).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void updateCoursConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.put("/cours/1/update").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr").param("titre", "Cours sur les tests JUNIT")
				.param("description", "Cours portant sur comment bien réaliser ces tests avec JUNIT")
				.param("prix", "45.5")).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void updateCoursInexistantConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.put("/cours/99/update").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr").param("titre", "Cours sur les tests JUNIT")
				.param("description", "Cours portant sur comment bien réaliser ces tests avec JUNIT")
				.param("prix", "45.5")).andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

	@Test
	public void accesEpisodeConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/cours/1/1").sessionAttr("id", Long.valueOf("1")).sessionAttr("email",
				"pierre.dupont@email.fr")).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void accesEpisodeInexistantConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/cours/1/99").sessionAttr("id", Long.valueOf("1")).sessionAttr("email",
				"pierre.dupont@email.fr")).andExpect(MockMvcResultMatchers.status().isNotFound());
	}

	@Test
	public void creerEpisodeConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/cours/1/create").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr").param("nom", "Nom de l'épisode")
				.param("lien", "youtube.com")).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void updateEpisodeConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.put("/cours/1/1/update").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr").param("nom", "Nom de l'épisode 1 ")
				.param("lien", "youtube.com/1")).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void updateEpisodeInexistantConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.put("/cours/1/99/update").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr").param("nom", "Nom de l'épisode")
				.param("lien", "youtube.com")).andExpect(MockMvcResultMatchers.status().isNotFound());
	}

	@Test
	public void updateEpisodeMauvaisCoursConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.put("/cours/1/13/update").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr").param("nom", "Nom de l'épisode")
				.param("lien", "youtube.com")).andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

	@Test
	public void updateEpisodeCoursInexistantConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.put("/cours/99/1/update").sessionAttr("id", Long.valueOf("1"))
				.sessionAttr("email", "pierre.dupont@email.fr").param("nom", "Nom de l'épisode")
				.param("lien", "youtube.com")).andExpect(MockMvcResultMatchers.status().isNotFound());
	}
}
