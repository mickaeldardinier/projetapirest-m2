package fr.ul.miage.m2.sid.plateformecours;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { PlateformeCoursApplication.class })
@AutoConfigureMockMvc
@WebAppConfiguration
@ActiveProfiles("test")
class ControllersNonConnecteTests {

	@Autowired
	private MockMvc mvc;

	/*
	 * Actions en étant pas connecté
	 */
	@Test
	public void accesUtilisateursNonConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/utilisateurs"))
				.andExpect(MockMvcResultMatchers.status().isForbidden());
	}

	@Test
	public void accesUtilisateurNonConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/utilisateurs/1"))
				.andExpect(MockMvcResultMatchers.status().isForbidden());
	}

	@Test
	public void accesListeCoursNonConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/cours")).andExpect(MockMvcResultMatchers.status().isForbidden());
	}

	@Test
	public void accesUnCoursNonConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/cours/1")).andExpect(MockMvcResultMatchers.status().isForbidden());
	}

	@Test
	public void creerUnCoursNonConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/cours/create"))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

	@Test
	public void updateUnCoursNonConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.put("/cours/1/update"))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

	@Test
	public void accesEpisodeNonConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/cours/1/1")).andExpect(MockMvcResultMatchers.status().isForbidden());
	}

	@Test
	public void creerEpisodeNonConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/cours/1/create"))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

	@Test
	public void updateEpisodeNonConnecte() throws Exception {
		mvc.perform(MockMvcRequestBuilders.put("/cours/1/1/update"))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
}
